//
//  MallAppDelegate.h
//  NSStringDrawingSize
//
//  Created by starrykai on 09/05/2016.
//  Copyright (c) 2016 starrykai. All rights reserved.
//

@import UIKit;

@interface MallAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
