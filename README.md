# NSStringDrawingSize

[![CI Status](http://img.shields.io/travis/starrykai/NSStringDrawingSize.svg?style=flat)](https://travis-ci.org/starrykai/NSStringDrawingSize)
[![Version](https://img.shields.io/cocoapods/v/NSStringDrawingSize.svg?style=flat)](http://cocoapods.org/pods/NSStringDrawingSize)
[![License](https://img.shields.io/cocoapods/l/NSStringDrawingSize.svg?style=flat)](http://cocoapods.org/pods/NSStringDrawingSize)
[![Platform](https://img.shields.io/cocoapods/p/NSStringDrawingSize.svg?style=flat)](http://cocoapods.org/pods/NSStringDrawingSize)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NSStringDrawingSize is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "NSStringDrawingSize"
```

## Author

starrykai, starryskykai@gmail.com

## License

NSStringDrawingSize is available under the MIT license. See the LICENSE file for more info.
