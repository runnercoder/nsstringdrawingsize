//
//  NSString+CalculateSize.h
//  Yundongli
//
//  Created by 吴恺 on 16/9/2.
//  Copyright © 2016年 wukai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface NSString (CalculateSize)
//计算字符串大小
- (CGSize)sizeWithlineBreakMode:(NSLineBreakMode)lineBreakMode
                  textAlignment:(NSTextAlignment)textAlignment
                           font:(UIFont *)font;
@end
