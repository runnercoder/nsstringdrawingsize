//
//  NSString+CalculateSize.m
//  Yundongli
//
//  Created by 吴恺 on 16/9/2.
//  Copyright © 2016年 wukai. All rights reserved.
//

#import "NSString+StringSize.h"

@implementation NSString (CalculateSize)
- (CGSize)sizeWithlineBreakMode:(NSLineBreakMode)lineBreakMode
                  textAlignment:(NSTextAlignment)textAlignment
                           font:(UIFont *)font {
  NSMutableParagraphStyle *paragraphStyle =
      [[NSMutableParagraphStyle alloc] init];
  paragraphStyle.lineBreakMode = lineBreakMode;
  paragraphStyle.alignment = textAlignment;

  NSDictionary *attributes = @{
    NSFontAttributeName : font,
    NSParagraphStyleAttributeName : paragraphStyle
  };

  CGSize contentSize =
      [self boundingRectWithSize:CGSizeMake(0, 0)
                         options:(NSStringDrawingUsesLineFragmentOrigin |
                                  NSStringDrawingUsesFontLeading)
                      attributes:attributes
                         context:nil]
          .size;
  return contentSize;
}
@end
