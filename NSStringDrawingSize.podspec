#
#  Be sure to run `pod spec lint NSStringDrawingSize.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "NSStringDrawingSize"
  s.version      = "0.0.1"
  s.summary      = "size of string"
  s.description  = <<-DESC
                  根据string font计算字符串大小
                   DESC
  s.homepage     = "https://bitbucket.org/runnercoder/nsstringdrawingsize"
  s.license      = "MIT"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author             = { "starrykai" => "starryskykai@gmail.com" }
  s.platform     = :ios
  s.platform     = :ios, "8.0"
  s.source       = { :git => "https://runnercoder@bitbucket.org/runnercoder/nsstringdrawingsize.git", :tag => "#{s.version}" }
  s.source_files  = "Classes", "NSStringDrawingSize/Classes/*.{h,m}"
  s.framework  = "UIKit"
  s.requires_arc = true

end
