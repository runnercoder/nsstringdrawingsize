//
//  main.m
//  NSStringDrawingSize
//
//  Created by starrykai on 09/06/2016.
//  Copyright (c) 2016 starrykai. All rights reserved.
//

@import UIKit;
#import "DemoAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DemoAppDelegate class]));
    }
}
